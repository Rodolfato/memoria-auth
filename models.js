const task = [
    "id",
    "description",
    "status",
    "start",
    "end",
    "area",
    "user",
    "name",
    "grade",
    "unit",
    "attached",
    "goal"
]

const report = [
    "id",
    "description"
]

exports.task = task
exports.report = report