function numberToAlphabet(column){
    const alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I','J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    const alphabetTimes = Math.ceil(column/25)
    var columnInLetters = ''
    for(let i = 0; i < alphabetTimes; i++){
        columnInLetters += alphabet[column - 25 < 0 ? column : column - 25]
    }
    console.log("columnInLetters = " + columnInLetters)
    return columnInLetters
}



exports.numberToAlphabet = numberToAlphabet