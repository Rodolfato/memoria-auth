const express = require('express')
const app = express()
const models = require('./models')
const utils = require('./utils')

const { GoogleSpreadsheet } = require('google-spreadsheet')
const creds = require('./kenos-onawo-b61152db8bf4.json')

//const doc = new GoogleSpreadsheet('16iDle79h7Lh9RNbFXlywn-TRlo9kScTKll4j5ce9Uvo')

app.listen(3000)
app.use(express.json());

app.get('/', (req, res) => {
    console.log(models.task[0])
    res.send('Hi')

})

app.put('/task', async (req, res) => {
    try {
        const doc = new GoogleSpreadsheet(req.body.idSheet)
        await doc.useServiceAccountAuth(creds)
        await doc.loadInfo()
        const sheet = doc.sheetsByIndex[0]
        const id = req.body.id
        for(let key in req.body){
            if(key != 'id' && key != 'idSheet'){
                //console.log(key, req.body[key])
                //Index of element to update in models array
                indexOfElement = models.task.indexOf(key)
                //Data to insert to spreadsheet
                dataToInsert = req.body[key]
                console.log(`id: ${id}, dataToInsert: ${dataToInsert}, columnId: ${indexOfElement}`)
                await sheet.loadCells(`A1:${utils.numberToAlphabet(indexOfElement + 1)}${id + 1}`)
                console.log(sheet.getCell(id, indexOfElement).formattedValue)
                var cellToChange = sheet.getCell(id, indexOfElement)
                cellToChange.value = Number(dataToInsert)
                console.log(cellToChange.numberFormat)
                await sheet.saveUpdatedCells()
            }
        }
        res.status(200).send(req.body)
    } catch(e) {
        console.log(e)
    }
})

app.post('/report', async (req, res) => {
    try {
        const doc = new GoogleSpreadsheet(req.body.idSheet)
        await doc.useServiceAccountAuth(creds)
        await doc.loadInfo()
        const sheet = doc.sheetsByIndex[0]
        const rows = await sheet.getRows()
        console.log(rows.length)
        const id = rows.length
        await sheet.addRow({
            id: id + 1,
            description: req.body.description,
            type: req.body.type
        })
        res.status(200).send(req.body)
    } catch(e) {
        console.log(e)
    }
})